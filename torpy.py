#!/usr/bin/env python
import sys
import os
import requests
import json
import time
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

class MainApp(QMainWindow):
	def __init__(self , parent=None):
		super().__init__()
		uic.loadUi("src/gui.ui", self)
		self.setFixedSize(350, 300)
		# Connect Button :
		self.Connect_Button.clicked.connect(self.torconnect)
		self.Connect_Button.clicked.connect(self.net_stat)
		# Disconnect Button :
		self.Disconnect_Button.clicked.connect(self.tordisconnect)
		self.Disconnect_Button.clicked.connect(self.net_stat)
		# Status :
		self.net_stat()
		# Systemtray :
		self.systemtray()
  
	# Connection Status :	
	def net_stat(self):
		tor_req = requests.get("https://check.torproject.org/api/ip")
		ip = tor_req.json()['IP']
		status = tor_req.json()['IsTor']
		
		# Val :
		if status == True:
			# IP :
			self.ipstat.setStyleSheet("color : lightgreen")
			self.ipstat.setText(ip)
			# Status :
			con = ("Connected")
			self.connstat.setStyleSheet("color : lightgreen")
			self.connstat.setText(con)
			self.img.setStyleSheet("image : url(src/images/tor-on.png);")
			
		elif status == False:
			# IP :
			self.ipstat.setStyleSheet("color : red")
			self.ipstat.setText(ip)
			# Status :
			con = ("Not Connect")
			self.connstat.setStyleSheet("color : red")
			self.connstat.setText(con)
			self.img.setStyleSheet("image : url(src/images/tor-off.png);")

		
	# Connect :
	def torconnect(self):
		start_tor = "pkexec systemctl restart tor && pkexec ./src/script/tor-router"
		os.system(start_tor)
		notif = 'notify-send "Connected Through Tor  🧅" '
		os.system(notif)
		
	# Disconnect :
	def tordisconnect(self):
		stop_tor = "pkexec systemctl restart iptables && pkexec systemctl restart tor"
		os.system(stop_tor)
		notif = 'notify-send "Disconnect From Tor 🧅" '
		os.system(notif)
  
	# Systemtray :
	def systemtray(self):
		trayicon = QIcon("src/trayicons/tor.svg") 
		self.systray = QSystemTrayIcon(trayicon, self)
		self.systray.setVisible(True)

		# Creating the options
		self.menu = QMenu(self)
  
		#Show App :
		show_app_icon = QIcon("src/trayicons/chevrons-up.svg") 
		self.show_app  = QAction(show_app_icon, "Show")
		self.menu.addAction(self.show_app)
		self.show_app.triggered.connect(self.show)
  
		#Hide App :
		hide_app_icon = QIcon("src/trayicons/chevrons-down.svg")
		self.hide_app = QAction(hide_app_icon, "Hide")
		self.menu.addAction(self.hide_app)
		self.hide_app.triggered.connect(self.hide)
    
		# Connect :
		connect_icon = QIcon("src/trayicons/shield.svg")
		self.connect = QAction(connect_icon, "Connect")
		self.menu.addAction(self.connect)
		self.connect.triggered.connect(self.torconnect)
		self.connect.triggered.connect(self.net_stat)	

		# Disonnect :
		disonnect_icon = QIcon("src/trayicons/shield-off.svg")
		self.disonnect = QAction(disonnect_icon, "Disonnect")
		self.menu.addAction(self.disonnect)
		self.disonnect.triggered.connect(self.tordisconnect)
		self.disonnect.triggered.connect(self.net_stat)

		# To quit the app
		quit_icon = QIcon("src/trayicons/log-out.svg")
		self.quit = QAction(quit_icon, "Quit")
		self.menu.addAction(self.quit)
		self.quit.triggered.connect(app.quit)

		# Adding options to the System Tray
		self.systray.setContextMenu(self.menu)
		self.systray.show()

 
app = QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)
window = MainApp()
window.show()
app.exec_()
